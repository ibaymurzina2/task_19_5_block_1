﻿#include <iostream>
using namespace std;

class Animal
{
public:
   
  virtual  void Voice()
    {
        cout << "Animals talk\n";
    }
};

class Dog : public Animal
{
public:
    
     void Voice() override
    {
        cout << "Dog talk: WOOF!\n";
    }
};

class Cat :public Animal
{
public:
    void Voice() override
    {
        cout << "Cat talk: MEOW!\n";
    }
};

class Mouse : public Animal
{
public:
    void Voice() override
    {
        cout << "Mouse talk: PI-PI-PI!\n";
    }
};
int main()
{ 
    const int sizeArray = 3;
    int size = sizeArray;
    Animal* pArrayAnimal[sizeArray] = {new Dog, new Cat,  new Mouse};

    int i = 0;
    while (i < size) {
        pArrayAnimal[i]->Voice();
        i++;
   }
          
    return 0;
}

